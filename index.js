const { STSClient, GetCallerIdentityCommand } = require("@aws-sdk/client-sts");

// Create STS (Security Token Service) client
const stsClient = new STSClient({
	region: process.env.AWS_DEFAULT_REGION
});

// Get caller identity (whoami for AWS API services)
const callerCommand = new GetCallerIdentityCommand({});

stsClient.send(callerCommand)
.then((data) => {
	// Process data
	console.log(data);
})
.catch((error) => {
	// Error handling
	console.warn(error);
})
.finally(() => {
    // Clean environment
	stsClient.destroy();
});